import React from 'react';
import {useState} from "react";
import axiosApi from "../../axiosApi";
import dayjs from "dayjs";
import './AddForm.css'


const AddForm = ({history}) => {
    const [post, setPost] = useState({
        Title: '',
        Description: '',
        Data: dayjs().format('DD/MM/YYYY h:mm A'),
    });

    const onInputChange = e => {
        const {name, value} = e.target;

        setPost(prev => ({
            ...prev,
            [name]: value
        }))
    };

    const createPost = async e => {
        e.preventDefault();
        try {
            await axiosApi.post('/posts.json', {
                post
            });
        } finally {
            history.replace('/');
        }
    };

    return (
        <div className="PostForm">
            <form onSubmit={createPost}>
                <input className="Input"
                       name="Title"
                       placeholder="Title"
                       value={post.title}
                       onChange={onInputChange}/>
                <textarea className="Input"
                          name="Description"
                          placeholder="Write something"
                          value={post.description}
                          onChange={onInputChange}/>
                <button type="submit">SAVE</button>
            </form>
        </div>
    );
};

export default AddForm;