import React from 'react';
import {NavLink} from "react-router-dom";
import './Header.css'

const Header = () => {
    return (
        <div className="Header">
            <h1>My page</h1>
            <div>
                <NavLink className='HeaderLink' to='/'>Home</NavLink>
                <NavLink className='HeaderLink' to='/add'>Add</NavLink>
                <NavLink className='HeaderLink' to='/about'>About</NavLink>
                <NavLink className='HeaderLink' to='/contacts'>Contacts</NavLink>
            </div>

        </div>
    );
};

export default Header;