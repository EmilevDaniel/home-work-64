import React from 'react';
import {NavLink} from "react-router-dom";
import './Post.css'

const Post = ({posts}) => {
    let url = `posts/${posts[0]}.json`;
    return (
        <div className='Post'>
            <p>Created on: {posts[1].post.Data}</p>
            <h1>{posts[1].post.Title}</h1>
            <NavLink className='PostButton' to={url}>READ MORE =></NavLink>
        </div>
    );
};

export default Post;