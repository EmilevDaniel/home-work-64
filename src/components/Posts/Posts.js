import React from 'react';
import {useState, useEffect} from "react";
import axiosApi from "../../axiosApi";
import Post from "../Post/Post";

const Posts = () => {
    const [newPosts, setNewPosts] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            const response = await axiosApi.get('/posts.json');
            setNewPosts(Object.entries(response.data));
        };
        fetchData().catch(e => console.error(e));
    }, []);

    return (
        <div>
            <div className='PostsHolder'>
                {newPosts.map((data, index) => {
                    return (<Post key={index} posts={data}/>);
                })}
            </div>
        </div>
    );
};

export default Posts;