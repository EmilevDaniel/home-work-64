import React from 'react';
import Header from "../components/Header/Header";
import Posts from "../components/Posts/Posts";

const MainPage = () => {
    return (
        <div>
            <Header/>
            <Posts/>
        </div>
    );
};

export default MainPage;