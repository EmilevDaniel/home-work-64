import React from 'react';

const About = () => {
    return (
        <div>
            <div>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium, autem dolorum error
                    exercitationem explicabo illo magnam numquam porro provident ratione repellendus tempore velit
                    voluptate! Fugit, minima sapiente.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At minus vitae voluptatem!</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem distinctio, ex fugit iure iusto
                    officiis quaerat similique totam? Adipisci, animi asperiores culpa eos eum libero nam nihil officiis
                    placeat repellendus saepe sed temporibus veniam. Accusamus accusantium atque blanditiis deleniti,
                    dignissimos dolore ea, eaque eligendi enim est exercitationem illo iure nesciunt nobis odio omnis
                    pariatur perspiciatis quaerat quam quod reiciendis repellendus sapiente sint vel veritatis vero.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum, similique.</p>
            </div>
        </div>
    );
};

export default About;