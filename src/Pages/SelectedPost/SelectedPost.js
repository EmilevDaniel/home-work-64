import React from 'react';
import {useState, useEffect} from "react";
import axiosApi from "../../axiosApi";
import Header from "../../components/Header/Header";

const SelectedPost = ({match}) => {
    const [singlePost, setSinglePost] = useState(null);

    useEffect(() => {
        const fetchData = async () => {
            const response = await axiosApi.get('/posts/' + match.params.id);
            setSinglePost(Object.values(response.data));
        };

        fetchData().catch(console.error)
    }, [match.params.id]);


    return singlePost &&
        (<>
            <Header/>
            <div className='Post'>
                <p>{singlePost[0].Data}</p>
                <h1> {singlePost[0].Title}</h1>
                <p>{singlePost[0].Description}</p>
            </div>
        </>)
};

export default SelectedPost;