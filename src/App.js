import React from 'react';
import MainPage from "./Pages/MainPage";
import './App.css';

import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import About from "./Pages/About";
import Add from "./Pages/Add";
import SelectedPost from "./Pages/SelectedPost/SelectedPost";

function App() {
    return (
        <div className="App">
            <Router>
                <Switch>
                    <Route exact path="/" component={MainPage}/>
                    <Route exact path="/about" component={About}/>
                    <Route exact path="/add" component={Add}/>
                    <Route exact path="/contacts" component={About}/>
                    <Route exact path="/posts/:id" component={SelectedPost}/>
                </Switch>
            </Router>
        </div>
    );
}

export default App;
